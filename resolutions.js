Resolutions = new Mongo.Collection('resolutions');

if (Meteor.isClient) {
  Meteor.subscribe("resolutions");

  Template.body.helpers({
    resolutions: function(){
      if (Session.get('hideFinished')){
        return Resolutions.find({checked:{$ne: true}});
      }else{
        return Resolutions.find();
      }
    },
    hideFinished: function(){
      return Session.get('hideFinished');
    },
  });
  Template.body.events({
    'submit .new-resolution': function(event){
      var title = event.target.title.value;
      
      Meteor.call('addResolution', title, function (error, result) {});

      event.target.title.value = "";

      return false;
    },
    'change .hide-finished': function(event){
      Session.set('hideFinished', event.target.checked);
    }
  });

  // Accounts.ui.config({
  //   passwordSignupFields: "USERNAME_ONLY"
  // });
  // Accounts.config({
  //   sendVerificationEmail: true,
  //   forbidClientAccountCreation: true,
  //   restrictCreationByEmailDomain: "school.edu",
  //   loginExpirationDays: 30,
  //   oauthSecretKey: "wgporjigrpqgdfg",
  // });
  // Accounts.ui.config({
  //   requestPermissions: {},
  //   requestOfflineToken: {},
  //   passwordSignupFields: "USERNAME_AND_OPTIONAL_EMAIL",
  // });

  Template.loginButtons.rendered = function(event){
    var elem = $('#login-sign-in-link');
    elem.removeClass('login-link-text');
    elem.addClass('login gylphicon gylphicon-user');
  };
}

if (Meteor.isServer) {
    Meteor.startup(function () {
        // code to run on server at startup
    });
    Meteor.publish("resolutions", function () {
       return Resolutions.find({
        $or: [
          {private: {$ne: true}},
          {owner: this.userId}
        ]
       });
    });
}

Meteor.methods({
  addResolution: function (title) {
    if( title === "" ){
      alert("The Resoultions must not be empty");
      return false;
    }
    Resolutions.insert({
      title: title,
      createdAt: new Date(),
      owner: Meteor.userId()
    });
  },
  deleteResolution: function  (id) {

    var res = Resolutions.findOne(id);

    if(res.owner !== Meteor.userId()){
       throw Meteor.Error("not autorized");
    }

    Resolutions.remove(id);
  },
  updateResolution: function  (id,checked) {

    var res = Resolutions.findOne(id);

    if(res.owner !== Meteor.userId()){
       throw Meteor.Error("not autorized");
    }

    Resolutions.update(id,{$set: {checked: checked}});
  },
  setPrivate: function (id, private) {

    var res = Resolutions.findOne(id);

    if(res.owner !== Meteor.userId()){
       throw Meteor.Error("not autorized");
    }

    Resolutions.update(id, {$set: { private: private}}); 
  }
});